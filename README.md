# Lossycop (alpha version)

Lossycop is a tool to detect lossy-to-lossless transcodes.
Currently, only transcodes from MP3 files are supported,
with more to be added in the future.

Example usage: 
```bash
$ lossycop test1.flac test2.flac 
test1.flac: lossy
test2.flac: lossless
```

The full set of command line options can be obtained using `lossycop --help`.
On the first run of lossycop, it will prompt you whether to download the ML model
from the internet. You can either let it download it automatically or download it
yourself from https://gitlab.com/lossycop/lossycop-models and place it in
`~/.lossycop/<filename>.h5`. 

## Troubleshooting

* If you experience issues, you can run lossycop with the `-v` flag and
open an issue in GitLab. Please include OS version, Python version and the output
from running `lossycop -v yourfile.flac`.
* If you receive a wrong prediction, here's how you can help lossycop improve:
  * Run lossycop with -v
  * Upload the wav/flac file to an online file hosting service.
  * Open a GitLab issue with those two things.
* Missing options, documentation, features, etc. - please open a GitLab issue
  describing your use-case.

## Installation

[Options for customization](#installation-options)

### Linux
Lossycop requires Python 3 and can be easily installed via pip.
Currently, it works by downloading the master version from GitLab.
An official package published on pypi is coming soon.

#### Ubuntu (global install):
```bash
sudo apt install python3-pip  # Might already be installed
wget "https://gitlab.com/lossycop/lossycop/-/archive/master/lossycop-master.zip"
sudo pip3 install lossycop-master.zip
```

#### Ubuntu (virtualenv install)
```bash
sudo apt install python3-pip
sudo pip3 install virtualenv
# cd into the directory of your choice
virtualenv -p python3 venv
source venv/bin/activate
wget "https://gitlab.com/lossycop/lossycop/-/archive/master/lossycop-master.zip"
pip install lossycop-master.zip
```

#### Arch
```bash
sudo pacman -S wget python-pip libsndfile
wget "https://gitlab.com/lossycop/lossycop/-/archive/master/lossycop-master.zip"
sudo pip install lossycop-master.zip
```

### Windows (still untested, contributions welcome)

1. Install Python 3.6 or 3.7 from the official Python site: https://www.python.org/downloads/windows/
   . You probably want the `Windows x86-64 executable installer`. Install it and make sure to check
   the `Add Python 3.x to PATH` option.
2. Download lossycop from GitLab: https://gitlab.com/lossycop/lossycop/-/archive/master/lossycop-master.zip
3. After the install completes, start a command line as Administrator and perform the following steps:
    1. `cd` into your Downloads directory
    2. `pip install lossycop-master.zip`
 
#### Installation customization options/tips {#installation-options}

1. The global installation is probably the easiest, but doesn't provide any isolation.
   if other programs require different version of the packages lossycop depends on,
   there could be a conflict. Installing into a virtualenv is safe and isolated.
1. Instead of downloading the zip file of master, you can clone the repository. This will
   probably make updates easier. So instead of `wget ...` you can `git clone https://gitlab.com/lossycop/lossycop.git`
   , then `pip install /path/to/clone/lossycop`. To update, `git pull` and run
   the same `pip install ...` again.

### How does lossycop work?

Lossycop uses machine learning (a neural network) to understand what lossy vs lossless
music looks like. For the dataset, a large number of lossless music files are sliced into 0.5s pieces
and this constitutes the lossless class. Additionally, they are transcoded using lame to a number
of different bitrates, sliced the same way and that goes into the lossy class.

The architecture of the network is two STFTs (512 window length and 2048 window length) that
feed into a number of convolution/max pooling layers. The resulting layers are flattened,
concatenated and fed into a fully-connected layer.

For prediction, a number of 0.5s slices are extracted from the files, run through the network
and a prediction is made based on how many slices fall into which class.
