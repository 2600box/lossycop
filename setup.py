from distutils.core import setup

setup(
    name='lossycop',
    description='A tool to detect lossy to lossless transcodes.',
    version='0.0.2',
    packages=['lossycop'],
    entry_points={
        'console_scripts': [
            'lossycop=lossycop.lossycop:main',
        ],
    },
    install_requires=[
        'SoundFile>=0.10.2<1.0.0',
        'numpy>=1.16.3<2.0.0',
        'tensorflow>=1.13.1<2.0.0',
        'requests>=2.21.0<3.0.0',
        'resampy>=0.2.1<1.0.0',
    ]
)
