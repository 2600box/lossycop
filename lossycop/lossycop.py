#!/usr/bin/env python3

import argparse
import json
import logging
import os
import time
from itertools import count

import numpy as np
import requests
import soundfile

from lossycop.common import SLICE_NUM_SAMPLES, cut_audio, BASE_DIR, get_model, CLASS_LOSSLESS, CLASS_LOSSY, \
    ensure_redbook, CLASS_QUIET

logger = logging.getLogger(__name__)

MODEL_VERSION = 'v0.0.2'

MODEL_FILENAME = '{}.weights.h5'.format(MODEL_VERSION)
MODEL_PATHS = [
    os.path.join(BASE_DIR, MODEL_FILENAME),
    os.path.join(os.path.expanduser('~'), '.lossycop', MODEL_FILENAME)
]

MAX_SLICES = 64
MIN_GOOD_SLICES = 20
MIN_NUM_LOSSLESS_SLICES = 10
LOSSLESS_THRESHOLD = 0.6

RESULT_ERROR_DECODING = 'error_decoding'
RESULT_NOT_ENOUGH_GOOD_SLICES = 'not_enough_good_slices'
RESULT_LOSSLESS = 'lossless'
RESULT_LOSSY = 'lossy'

FORMAT_TEXT = 'text'
FORMAT_JSON = 'json'
FORMATS = {
    FORMAT_TEXT,
    FORMAT_JSON,
}

URL_DOWNLOAD_URL = 'https://gitlab.com/lossycop/lossycop-models/raw/master/{}.url.txt?inline=false'.format(
    MODEL_VERSION)

EXIT_SUCCESS = 0
EXIT_DETECTED_BAD_FILES = 1
EXIT_ERROR_FINDING_MODEL = 2


def download_model():
    logger.debug('Fetching URL file {}'.format(URL_DOWNLOAD_URL))
    url_resp = requests.get(URL_DOWNLOAD_URL)
    if url_resp.status_code != 200:
        print('Unable to download URL file: {}'.format(url_resp.status_code))
        exit(EXIT_ERROR_FINDING_MODEL)
    weights_url = url_resp.text.strip()
    logger.debug('Fetching weights file from {}'.format(weights_url))
    weights_resp = requests.get(weights_url)
    if weights_resp.status_code != 200:
        print('Unable to weights file: {}'.format(weights_resp.status_code))
        exit(EXIT_ERROR_FINDING_MODEL)
    for model_path in MODEL_PATHS:
        try:
            os.makedirs(os.path.dirname(model_path), exist_ok=True)
            with open(model_path, 'wb') as f:
                f.write(weights_resp.content)
            print('Saved model to {}'.format(model_path))
            return model_path
        except PermissionError:
            continue
    print('Unable to find a suitable location for weights file. Tried: {}'.format(MODEL_PATHS))
    exit(EXIT_ERROR_FINDING_MODEL)


def load_model():
    model_path = None
    for model_path_option in MODEL_PATHS:
        if os.path.exists(model_path_option):
            model_path = model_path_option
            break
    else:
        answer = input('Model file not found. Do you want to download it? [y/N]: ')
        if answer == 'y':
            model_path = download_model()
        else:
            exit(2)

    load_start = time.time()
    model = get_model()
    model.load_weights(model_path)
    logger.debug('Completed loading model in %f', time.time() - load_start)
    return model


def cut_slices(data, samplerate):
    num_samples = data.shape[0]
    num_slices = min(MAX_SLICES, num_samples // SLICE_NUM_SAMPLES)
    return cut_audio(data, samplerate, num_slices)


def get_predictions(model, data, samplerate):
    slices = cut_slices(data, samplerate)
    return model.predict(np.array(slices), batch_size=32)


def init_env(args):
    os.putenv('CUDA_VISIBLE_DEVICES', '-1')
    np.set_printoptions(precision=5, suppress=True)
    if args.verbose:
        log_level = logging.DEBUG
    else:
        os.putenv('TF_CPP_MIN_LOG_LEVEL', '3')
        logging.getLogger('tensorflow').setLevel(logging.CRITICAL)
        log_level = logging.ERROR

    logging.basicConfig(
        level=log_level,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    )


def process_file(model, file_path):
    try:
        logger.debug('Reading %s', file_path)
        data, samplerate = soundfile.read(file_path)
        if len(data.shape) == 1:  # Mono files, convert to stereo
            logger.debug('Converting stereo to mono...')
            data = np.transpose(np.array([data, data]))
        elif data.shape[1] != 2:
            logger.error('Currently only stereo (2 channel) files are supported. {} has {} channels.'.format(
                file_path, data.shape[1]))
            return RESULT_ERROR_DECODING
        data, samplerate = ensure_redbook(data, samplerate)
    except Exception:
        if logger.isEnabledFor(logging.DEBUG):
            logger.exception('Error decoding {}'.format(file_path))
        return RESULT_ERROR_DECODING

    logger.debug('Getting predictions for %s', file_path)
    prediction_start = time.time()
    predictions = get_predictions(model, data, samplerate)
    logger.debug('Received predictions in %f', time.time() - prediction_start)

    if logger.isEnabledFor(logging.DEBUG):
        for i, prediction in zip(count(), predictions):
            print('Piece {} prediction {}'.format(i, prediction))

    num_lossless = 0
    num_lossy = 0
    for prediction in predictions:
        if prediction[CLASS_LOSSLESS] > LOSSLESS_THRESHOLD:
            num_lossless += 1
        elif prediction[CLASS_LOSSY] > prediction[CLASS_QUIET]:
            num_lossy += 1
    logger.debug('Num Lossless: %s', num_lossless)
    logger.debug('Num Lossy: %s', num_lossy)

    if num_lossless + num_lossy < MIN_GOOD_SLICES:
        return RESULT_NOT_ENOUGH_GOOD_SLICES

    if num_lossless >= MIN_NUM_LOSSLESS_SLICES:
        return RESULT_LOSSLESS

    return RESULT_LOSSY


def print_text(results):
    for file_path, result in results.items():
        if result == RESULT_ERROR_DECODING:
            result_str = 'error decoding file'
        elif result == RESULT_NOT_ENOUGH_GOOD_SLICES:
            result_str = 'too few loud enough slices'
        elif result == RESULT_LOSSLESS:
            result_str = 'lossless'
        elif result == RESULT_LOSSY:
            result_str = 'lossy'
        else:
            raise Exception('Unknown result')

        print('{}: {}'.format(
            os.path.basename(file_path),
            result_str,
        ))


def print_json(results):
    print(json.dumps(results, indent=4))


def main():
    supported_formats = ', '.join(soundfile.available_formats())

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'audio_file',
        nargs='+',
        help='Path to an audio file(s) to test. Supported formats are: {}'.format(supported_formats),
    )
    parser.add_argument(
        '--verbose',
        '-v',
        help='Enable printing debug messages.',
        default=False,
        action='store_true',
    )
    parser.add_argument(
        '--format',
        '-f',
        help='Select the format for data output.',
        default=FORMAT_TEXT,
        choices=FORMATS,
    )
    args = parser.parse_args()
    init_env(args)

    model = load_model()

    results = {file_path: process_file(model, file_path) for file_path in args.audio_file}
    if args.format == FORMAT_TEXT:
        print_text(results)
    elif args.format == FORMAT_JSON:
        print_json(results)
    else:
        raise Exception('Unknown format')

    if all(v in {RESULT_NOT_ENOUGH_GOOD_SLICES, RESULT_LOSSLESS} for v in results.values()):
        exit(EXIT_SUCCESS)
    else:
        exit(EXIT_DETECTED_BAD_FILES)


if __name__ == '__main__':
    main()
